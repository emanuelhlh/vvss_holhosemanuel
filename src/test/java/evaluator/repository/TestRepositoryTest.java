package evaluator.repository;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.util.InputValidation;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static org.junit.Assert.*;

public class TestRepositoryTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    // ne valid path 1
    public void test1() {

        TestRepository testRepository = new TestRepository();
        IntrebariRepository repository = new IntrebariRepository();
        repository.setIntrebari(null);

        // TEST 1
        try {
            evaluator.model.Test test =  testRepository.createTest(repository);
        } catch(Exception ex){
            String message = "Nu exista intrebari adaugate!";
            assertEquals(message, ex.getMessage());
        }
    }

    @Test
    // ne valid path 2
    public void test2() {

        TestRepository testRepository = new TestRepository();
        IntrebariRepository repository = new IntrebariRepository();
        List<Intrebare> intrebari = repository.getIntrebari();
        intrebari.remove(0);
        intrebari.remove(0);
        intrebari.remove(0);
        repository.setIntrebari(intrebari);
        // TEST 2
        try {
            evaluator.model.Test test =  testRepository.createTest(repository);
        } catch(Exception ex){
            String message = "Trebuie sa existe cel putin 5 intrebari!";
            assertEquals(message, ex.getMessage());
        }
    }

    @Test
    // ne valid path 3
    public void test3() {

        TestRepository testRepository = new TestRepository();
        IntrebariRepository repository = new IntrebariRepository();
        List<Intrebare> intrebari = repository.getIntrebari();

        for (Intrebare intrebare : intrebari) {
            intrebare.setDomeniu("Domeniu identic");
        }

        repository.setIntrebari(intrebari);
        // TEST 3
        try {
            evaluator.model.Test test =  testRepository.createTest(repository);
        } catch(Exception ex){
            String message = "Trebuie sa existe intrebari din cel putin 5 domenii diferite";
            assertEquals(message, ex.getMessage());
        }
    }

    @Test
    // ne valid path 4
    public void test4() {

        TestRepository testRepository = new TestRepository();
        IntrebariRepository repository = new IntrebariRepository();
        List<Intrebare> intrebari = repository.getIntrebari();

        // TEST 4
        try {
            evaluator.model.Test test =  testRepository.createTest(repository);
            assertEquals(test.getIntrebari().size(),5);
        } catch(Exception ex){
            String message = "";
            assertEquals(message, ex.getMessage());
        }
    }









}