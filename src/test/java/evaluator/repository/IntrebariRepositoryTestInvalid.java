package evaluator.repository;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class IntrebariRepositoryTestInvalid {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();



    @Test
    // ne valid
    public void enuntDoesNotStartWithUppercase() throws DuplicateIntrebareException, InputValidationFailedException {
        IntrebariRepository repository = new IntrebariRepository();
        String enunt = "enunt";
        String varianta1 = "1)Varianta1";
        String varianta2 = "2)Varianta2";
        String varianta3 = "3)Varianta3";
        Integer variantaCorecta = 1;
        String domeniu = "General";
        Intrebare i = new Intrebare();
        i.setDomeniu(domeniu);
        i.setEnunt(enunt);
        i.setVarianta1(varianta1);
        i.setVarianta2(varianta2);
        i.setVarianta3(varianta3);
        i.setVariantaCorecta(variantaCorecta);

        try {
            repository.addIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
        } catch(InputValidationFailedException ex){
            String message = "Prima litera din enunt nu e majuscula!";
            assertEquals(message, ex.getMessage());
        }
    }
}
