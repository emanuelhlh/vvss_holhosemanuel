package evaluator.repository;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.util.InputValidation;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class BigBangTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();


    // test modul A
    @Test
    public void testA(){
        IntrebariRepositoryTest test = new IntrebariRepositoryTest();
        try {
            test.enuntLength2();
        } catch (DuplicateIntrebareException e) {
            e.printStackTrace();
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
        }
    }

    // test modul B
    @Test
    public void testB(){
        TestRepositoryTest test = new TestRepositoryTest();
        test.test1();
    }

    // test modul C
    @Test
    public void testC(){
        IntrebariRepositoryTest test = new IntrebariRepositoryTest();
        try {
            test.testF03valid();
        } catch (NotAbleToCreateStatisticsException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void integrationTestABC(){
        IntrebariRepository intrebariRepository = new IntrebariRepository();
        String domeniu = "Geometrie";
        String enunt = "Exemplu intrebare?";
        String v1 = "1)Varianta1";
        String v2 = "2)Varianta2";
        String v3 = "3)Varianta3";
        Integer r = 1;

        try {
            intrebariRepository.removeAllQuestions();
            intrebariRepository.addIntrebare(enunt, v1, v2, v3, r, domeniu);
            assertEquals(intrebariRepository.getIntrebari().size(),1);
        } catch (DuplicateIntrebareException e) {
            e.printStackTrace();
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
        }

        TestRepository testRepository = new TestRepository();
        try {
            intrebariRepository.addIntrebare(enunt+"a?", v1, v2, v3, r, domeniu+"a");
            intrebariRepository.addIntrebare(enunt+"b?", v1, v2, v3, r, domeniu+"b");
            intrebariRepository.addIntrebare(enunt+"c?", v1, v2, v3, r, domeniu+"c");
            intrebariRepository.addIntrebare(enunt+"d?", v1, v2, v3, r, domeniu+"d");
            intrebariRepository.addIntrebare(enunt+"e?", v1, v2, v3, r, domeniu+"e");

            evaluator.model.Test testCreat =  testRepository.createTest(intrebariRepository);
            assertEquals(testRepository.getTeste().size(),1);
            assertEquals(testCreat.getIntrebari().size(),5);
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
        } catch (DuplicateIntrebareException e) {
            e.printStackTrace();
        }

        AppController controller = new AppController();
        controller.setIntrebariRepository(intrebariRepository);
        try {
            Statistica statistica = controller.getStatistica();
            assertEquals(statistica.getIntrebariDomenii().size(),6);
            assertEquals(statistica.getIntrebariDomenii().get("Geometrie").toString(),"1");
            assertEquals(statistica.getIntrebariDomenii().get("Geometriea").toString(),"1");
            assertEquals(statistica.getIntrebariDomenii().get("Geometrieb").toString(),"1");
            assertEquals(statistica.getIntrebariDomenii().get("Geometriec").toString(),"1");
            assertEquals(statistica.getIntrebariDomenii().get("Geometried").toString(),"1");
            assertEquals(statistica.getIntrebariDomenii().get("Geometriee").toString(),"1");
        } catch (NotAbleToCreateStatisticsException e) {
            e.printStackTrace();
        }

    }

}