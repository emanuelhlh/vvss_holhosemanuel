package evaluator.repository;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.util.InputValidation;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class IntrebariRepositoryTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    // valid
    public void enuntStartsWithUppercase() throws DuplicateIntrebareException, InputValidationFailedException {
        IntrebariRepository repository = new IntrebariRepository();
        String enunt = "Enunt normal?";
        String varianta1 = "1)Varianta1";
        String varianta2 = "2)Varianta2";
        String varianta3 = "3)Varianta3";
        Integer variantaCorecta = 1;
        String domeniu = "General";
        Intrebare i = new Intrebare();
        i.setDomeniu(domeniu);
        i.setEnunt(enunt);
        i.setVarianta1(varianta1);
        i.setVarianta2(varianta2);
        i.setVarianta3(varianta3);
        i.setVariantaCorecta(variantaCorecta);

        Intrebare adaugata = repository.addIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);

        // TEST 1
        assertEquals(i, adaugata );
    }

    @Test
    // ne valid
    public void enuntDoesNotStartWithUppercase() throws DuplicateIntrebareException, InputValidationFailedException {
        IntrebariRepository repository = new IntrebariRepository();
        String enunt = "enunt";
        String varianta1 = "1)Varianta1";
        String varianta2 = "2)Varianta2";
        String varianta3 = "3)Varianta3";
        Integer variantaCorecta = 1;
        String domeniu = "General";
        Intrebare i = new Intrebare();
        i.setDomeniu(domeniu);
        i.setEnunt(enunt);
        i.setVarianta1(varianta1);
        i.setVarianta2(varianta2);
        i.setVarianta3(varianta3);
        i.setVariantaCorecta(variantaCorecta);

        try {
            repository.addIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
        } catch(InputValidationFailedException ex){
            String message = "Prima litera din enunt nu e majuscula!";
            assertEquals(message, ex.getMessage());
        }
    }

    @Test
    // valid
    public void enuntEndsWithQuestionMark() throws DuplicateIntrebareException, InputValidationFailedException {
        IntrebariRepository repository = new IntrebariRepository();
        String enunt = "Enunt?";
        String varianta1 = "1)Varianta1";
        String varianta2 = "2)Varianta2";
        String varianta3 = "3)Varianta3";
        Integer variantaCorecta = 1;
        String domeniu = "General";
        Intrebare i = new Intrebare();
        i.setDomeniu(domeniu);
        i.setEnunt(enunt);
        i.setVarianta1(varianta1);
        i.setVarianta2(varianta2);
        i.setVarianta3(varianta3);
        i.setVariantaCorecta(variantaCorecta);

        try {
            repository.addIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
        } catch(InputValidationFailedException ex){
            String message = "";
            assertEquals(message, ex.getMessage());
        }
    }

    @Test
    // ne valid
    public void enuntDoesNotEndWithQuestionMark() throws DuplicateIntrebareException, InputValidationFailedException {
        IntrebariRepository repository = new IntrebariRepository();
        String enunt = "Enunt";
        String varianta1 = "1)Varianta1";
        String varianta2 = "2)Varianta2";
        String varianta3 = "3)Varianta3";
        Integer variantaCorecta = 1;
        String domeniu = "General";
        Intrebare i = new Intrebare();
        i.setDomeniu(domeniu);
        i.setEnunt(enunt);
        i.setVarianta1(varianta1);
        i.setVarianta2(varianta2);
        i.setVarianta3(varianta3);
        i.setVariantaCorecta(variantaCorecta);

        try {
            repository.addIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
        } catch(InputValidationFailedException ex){
            String message = "Ultimul caracter din enunt nu e '?'!";
            assertEquals(message, ex.getMessage());
        }
    }

    @Test
    // valid
    public void varianta1StartsWithNumber() throws DuplicateIntrebareException, InputValidationFailedException {
        IntrebariRepository repository = new IntrebariRepository();
        String enunt = "Enunt?";
        String varianta1 = "1)Varianta1";
        String varianta2 = "2)Varianta2";
        String varianta3 = "3)Varianta3";
        Integer variantaCorecta = 1;
        String domeniu = "General";
        Intrebare i = new Intrebare();
        i.setDomeniu(domeniu);
        i.setEnunt(enunt);
        i.setVarianta1(varianta1);
        i.setVarianta2(varianta2);
        i.setVarianta3(varianta3);
        i.setVariantaCorecta(variantaCorecta);

        try {
            repository.addIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
        } catch(InputValidationFailedException ex){
            String message = "";
            assertEquals(message, ex.getMessage());
        }
    }

    @Test
    // ne valid
    public void varianta1DoesNotStartWithNumber() throws DuplicateIntrebareException, InputValidationFailedException {
        IntrebariRepository repository = new IntrebariRepository();
        String enunt = "Enunt?";
        String varianta1 = ")Varianta1";
        String varianta2 = "2)Varianta2";
        String varianta3 = "3)Varianta3";
        Integer variantaCorecta = 1;
        String domeniu = "General";
        Intrebare i = new Intrebare();
        i.setDomeniu(domeniu);
        i.setEnunt(enunt);
        i.setVarianta1(varianta1);
        i.setVarianta2(varianta2);
        i.setVarianta3(varianta3);
        i.setVariantaCorecta(variantaCorecta);

        try {
            repository.addIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
        } catch(InputValidationFailedException ex){
            String message = "Varianta1 nu incepe cu '1)'!";
            assertEquals(message, ex.getMessage());
        }
    }

    @Test
    // valid
    public void secondCharacterIsClosedParanthesis() throws DuplicateIntrebareException, InputValidationFailedException {
        IntrebariRepository repository = new IntrebariRepository();
        String enunt = "Enunt?";
        String varianta1 = "1)Varianta1";
        String varianta2 = "2)Varianta2";
        String varianta3 = "3)Varianta3";
        Integer variantaCorecta = 1;
        String domeniu = "General";
        Intrebare i = new Intrebare();
        i.setDomeniu(domeniu);
        i.setEnunt(enunt);
        i.setVarianta1(varianta1);
        i.setVarianta2(varianta2);
        i.setVarianta3(varianta3);
        i.setVariantaCorecta(variantaCorecta);

        try {
            repository.addIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
        } catch(InputValidationFailedException ex){
            String message = "";
            assertEquals(message, ex.getMessage());
        }
    }

    @Test
    // ne valid
    public void secondCharacterIsNotClosedParanthesis() throws DuplicateIntrebareException, InputValidationFailedException {
        IntrebariRepository repository = new IntrebariRepository();
        String enunt = "Enunt?";
        String varianta1 = "1Varianta1";
        String varianta2 = "2)Varianta2";
        String varianta3 = "3)Varianta3";
        Integer variantaCorecta = 1;
        String domeniu = "General";
        Intrebare i = new Intrebare();
        i.setDomeniu(domeniu);
        i.setEnunt(enunt);
        i.setVarianta1(varianta1);
        i.setVarianta2(varianta2);
        i.setVarianta3(varianta3);
        i.setVariantaCorecta(variantaCorecta);

        try {
            repository.addIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
        } catch(InputValidationFailedException ex){
            String message = "Varianta1 nu incepe cu '1)'!";
            assertEquals(message, ex.getMessage());
        }
    }

    @Test
    //  ne valid
    public void enuntLength0() throws DuplicateIntrebareException, InputValidationFailedException {
        IntrebariRepository repository = new IntrebariRepository();
        String enunt = "";
        String varianta1 = "1)Varianta1";
        String varianta2 = "2)Varianta2";
        String varianta3 = "3)Varianta3";
        Integer variantaCorecta = 1;
        String domeniu = "General";
        Intrebare i = new Intrebare();
        i.setDomeniu(domeniu);
        i.setEnunt(enunt);
        i.setVarianta1(varianta1);
        i.setVarianta2(varianta2);
        i.setVarianta3(varianta3);
        i.setVariantaCorecta(variantaCorecta);

        try {
            repository.addIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
        } catch(InputValidationFailedException ex){
            String message = "Enuntul este vid!";
            assertEquals(message, ex.getMessage());
        }
    }

    @Test
    //  ne valid
    public void enuntLength1() throws DuplicateIntrebareException, InputValidationFailedException {
        IntrebariRepository repository = new IntrebariRepository();
        String enunt = "A";
        String varianta1 = "1)Varianta1";
        String varianta2 = "2)Varianta2";
        String varianta3 = "3)Varianta3";
        Integer variantaCorecta = 1;
        String domeniu = "General";
        Intrebare i = new Intrebare();
        i.setDomeniu(domeniu);
        i.setEnunt(enunt);
        i.setVarianta1(varianta1);
        i.setVarianta2(varianta2);
        i.setVarianta3(varianta3);
        i.setVariantaCorecta(variantaCorecta);

        try {
            repository.addIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
        } catch(InputValidationFailedException ex){
            String message = "Ultimul caracter din enunt nu e '?'!";
            assertEquals(message, ex.getMessage());
        }
    }

    @Test
    //  valid
    public void enuntLength2() throws DuplicateIntrebareException, InputValidationFailedException {
        IntrebariRepository repository = new IntrebariRepository();
        String enunt = "A?";
        String varianta1 = "1)Varianta1";
        String varianta2 = "2)Varianta2";
        String varianta3 = "3)Varianta3";
        Integer variantaCorecta = 1;
        String domeniu = "General";
        Intrebare i = new Intrebare();
        i.setDomeniu(domeniu);
        i.setEnunt(enunt);
        i.setVarianta1(varianta1);
        i.setVarianta2(varianta2);
        i.setVarianta3(varianta3);
        i.setVariantaCorecta(variantaCorecta);

        try {
            repository.addIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
        } catch(InputValidationFailedException ex){
            String message = "";
            assertEquals(message, ex.getMessage());
        }
    }

    @Test
    //  ne valid
    public void variantaLength0() throws DuplicateIntrebareException, InputValidationFailedException {
        IntrebariRepository repository = new IntrebariRepository();
        String enunt = "A?";
        String varianta1 = "";
        String varianta2 = "2)Varianta2";
        String varianta3 = "3)Varianta3";
        Integer variantaCorecta = 1;
        String domeniu = "General";
        Intrebare i = new Intrebare();
        i.setDomeniu(domeniu);
        i.setEnunt(enunt);
        i.setVarianta1(varianta1);
        i.setVarianta2(varianta2);
        i.setVarianta3(varianta3);
        i.setVariantaCorecta(variantaCorecta);

        try {
            repository.addIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
        } catch(InputValidationFailedException ex){
            String message = "Varianta1 este vida!";
            assertEquals(message, ex.getMessage());
        }
    }

    @Test
    //  valid
    public void variantaLength2() throws DuplicateIntrebareException, InputValidationFailedException {
        IntrebariRepository repository = new IntrebariRepository();
        String enunt = "A?";
        String varianta1 = "1)";
        String varianta2 = "2)Varianta2";
        String varianta3 = "3)Varianta3";
        Integer variantaCorecta = 1;
        String domeniu = "General";
        Intrebare i = new Intrebare();
        i.setDomeniu(domeniu);
        i.setEnunt(enunt);
        i.setVarianta1(varianta1);
        i.setVarianta2(varianta2);
        i.setVarianta3(varianta3);
        i.setVariantaCorecta(variantaCorecta);

        try {
            repository.addIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
        } catch(InputValidationFailedException ex){
            String message = "Varianta1 este vida!";
            assertEquals(message, ex.getMessage());
        }
    }

    //F03-valid
    @Test
    public void testF03valid() throws NotAbleToCreateStatisticsException{
        IntrebariRepository repository = new IntrebariRepository();
        AppController controller = new AppController();
        controller.setIntrebariRepository(repository);
        Statistica statistica = controller.getStatistica();
        assertEquals(repository.getIntrebari().size(),4);
        assertEquals(statistica.getIntrebariDomenii().size(),2);
        assertEquals(repository.getIntrebariByDomain("Geometrie").size(),2);
        assertEquals(repository.getIntrebariByDomain("Algebra").size(),2);
    }

    //F03-nonvalid
    @Test
    public void testF03nonvalid() throws NotAbleToCreateStatisticsException{
        AppController controller = new AppController();
        controller.removeQuestions();
        try {
            Statistica statistica = controller.getStatistica();
        } catch(NotAbleToCreateStatisticsException ex){
            String message = "Repository-ul nu contine nicio intrebare!";
            assertEquals(message, ex.getMessage());
        }
    }



//
//    @Test
//    public void addIntrebare() throws DuplicateIntrebareException, InputValidationFailedException {
//
//        // parametrii alesi: enunt si varianta1
//
//        /**
//         * conditii enunt: nevid, incepe cu litera mare, ultimul caracter este '?'
//         *
//         * conditii varianta1: incepe cu un numar urmat de paranteza inchias ')'
//         */
//
//        IntrebariRepository repository = new IntrebariRepository();
//        String enunt = "Enunt normal?";
//        String varianta1 = "1)Varianta1";
//        String varianta2 = "2)Varianta2";
//        String varianta3 = "3)Varianta3";
//        Integer variantaCorecta = 1;
//        String domeniu = "General";
//        Intrebare i = new Intrebare();
//        i.setDomeniu(domeniu);
//        i.setEnunt(enunt);
//        i.setVarianta1(varianta1);
//        i.setVarianta2(varianta2);
//        i.setVarianta3(varianta3);
//        i.setVariantaCorecta(variantaCorecta);
//
//        String enunt2 = "Inca un enunt avalabil?";
//        String varianta11 = "1)Varianta valabila";
//
//        Intrebare ii = new Intrebare(enunt2, varianta11, varianta2, varianta3, variantaCorecta, domeniu);
//
//        Intrebare adaugata = repository.addIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
//        Intrebare adaugata2 = repository.addIntrebare(enunt2, varianta11, varianta2, varianta3, variantaCorecta, domeniu);
//
//        // TEST 1
//        assertEquals(i, adaugata );
//        // TEST 2
//        assertEquals(ii, adaugata2);
//        try{
//            repository.addIntrebare("", varianta1, varianta2, varianta3, variantaCorecta, domeniu);
//        }catch (InputValidationFailedException ex){
//            String message = "Enuntul este vid!";
//            // TEST 3
//            assertEquals(message, ex.getMessage());
//        }
//
//        try {
//            repository.addIntrebare("Enunt fara semnul intrebarii", varianta1, varianta2, varianta3, variantaCorecta, domeniu);
//        } catch(InputValidationFailedException ex){
//            String message = "Ultimul caracter din enunt nu e '?'!";
//            // TEST 4
//            assertEquals(message, ex.getMessage());
//        }
//
//        try {
//            repository.addIntrebare("enunt care incepe cu litera mica", varianta1, varianta2, varianta3, variantaCorecta, domeniu);
//        } catch(InputValidationFailedException ex){
//            String message = "Prima litera din enunt nu e majuscula!";
//            // TEST 5
//            assertEquals(message, ex.getMessage());
//        }
//
//        try {
//            repository.addIntrebare(enunt, "varianta fara numar", varianta2, varianta3, variantaCorecta, domeniu);
//        } catch(InputValidationFailedException ex){
//            String message = "Varianta1 nu incepe cu '1)'!";
//            // TEST 6
//            assertEquals(message, ex.getMessage());
//        }
//
//        // cazuri extreme (BVA)
//
//        // TEST 1 enunt format doar din semnul intrebarii - ne valid
//        try {
//            repository.addIntrebare("?", varianta1, varianta2, varianta3, variantaCorecta, domeniu);
//        } catch(InputValidationFailedException ex){
//            String message = "Prima litera din enunt nu e majuscula!";
//            assertEquals(message, ex.getMessage());
//        }
//
//        // TEST 2 enunt format doar dintr-o litera mare - ne valid
//        try {
//            repository.addIntrebare("A", varianta1, varianta2, varianta3, variantaCorecta, domeniu);
//        } catch(InputValidationFailedException ex){
//            String message = "Ultimul caracter din enunt nu e '?'!";
//            assertEquals(message, ex.getMessage());
//        }
//
//        // TEST 3 - enunt format dintr-o litera mare si semnul intrebarii - valid
//        try {
//            repository.addIntrebare("A?", varianta1, varianta2, varianta3, variantaCorecta, domeniu);
//        } catch(InputValidationFailedException ex){
//            String message = "";
//            assertEquals(message, ex.getMessage());
//        }
//
//        // TEST 4 - varianta formata doar ditr-o cifra - invalid
//        try {
//            repository.addIntrebare(enunt, "1", varianta2, varianta3, variantaCorecta, domeniu);
//        } catch(StringIndexOutOfBoundsException ex){
//            String message = "String index out of range: 1";
//            assertEquals(message, ex.getMessage());
//        }
//
//        // TEST 5 - varianta formata doar din cifra si paranteza - valid
//        try {
//            repository.addIntrebare(enunt, "1)", varianta2, varianta3, variantaCorecta, domeniu);
//        } catch(StringIndexOutOfBoundsException ex){
//            String message = "";
//            assertEquals(message, ex.getMessage());
//        }
//
//    }
}