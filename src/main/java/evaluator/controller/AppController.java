package evaluator.controller;

import java.util.LinkedList;
import java.util.List;

import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.model.Test;
import evaluator.repository.IntrebariRepository;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.repository.TestRepository;

public class AppController {
	
	private IntrebariRepository intrebariRepository;
	private TestRepository testRepository;
	
	public AppController() {
		intrebariRepository = new IntrebariRepository();
		testRepository = new TestRepository();
	}

	public void addNewTest(){
		try {
			testRepository.createTest(this.intrebariRepository);
		} catch (InputValidationFailedException e) {
			e.printStackTrace();
		}
	}

	public void setIntrebariRepository(IntrebariRepository intrebariRepository) {
		this.intrebariRepository = intrebariRepository;
	}

	//	public Intrebare addNewIntrebare(Intrebare intrebare) throws DuplicateIntrebareException{
//
//		intrebariRepository.addIntrebare(intrebare);
//
//		return intrebare;
//	}

	public Intrebare addNewIntrebare (String enunt, String varianta1, String varianta2, String varianta3,Integer variantaCorecta, String domeniu) throws DuplicateIntrebareException, InputValidationFailedException {
		Intrebare intrebareAdaugata = intrebariRepository.addIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
		return intrebareAdaugata;
	}

	public void removeQuestions(){
		intrebariRepository.removeAllQuestions();
	}
	
	public boolean existsQuestion(Intrebare intrebare){
		return intrebariRepository.existsQuestion(intrebare);
	}
	
	public Test createNewTest() throws NotAbleToCreateTestException{
		
		if(intrebariRepository.getIntrebari().size() < 3)
			throw new NotAbleToCreateTestException("Nu exista suficiente intrebari pentru crearea unui test!(5)");
		
		if(intrebariRepository.getNumberOfDistinctDomains() < 4)
			throw new NotAbleToCreateTestException("Nu exista suficiente domenii pentru crearea unui test!(5)");
		
		List<Intrebare> testIntrebari = new LinkedList<Intrebare>();
		List<String> domenii = new LinkedList<String>();
		Intrebare intrebare;
		Test test = new Test();
		
		while(testIntrebari.size() != 7){
			intrebare = intrebariRepository.pickRandomIntrebare();
			
//			if(testIntrebari.contains(intrebare) && !domenii.contains(intrebare.getDomeniu())){
//				testIntrebari.addStatistics(intrebare);
//				domenii.addStatistics(intrebare.getDomeniu());
//			}
			if(!testIntrebari.contains(intrebare)){
				testIntrebari.add(intrebare);
				if(!domenii.contains(intrebare.getDomeniu())){
					domenii.add(intrebare.getDomeniu());
				}
			}
			
		}
		
		test.setIntrebari(testIntrebari);
		return test;
		
	}
	
	public void loadIntrebariFromFile(String f){
		intrebariRepository.setIntrebari(intrebariRepository.loadIntrebariFromFile(f));
	}
	
	public Statistica getStatistica() throws NotAbleToCreateStatisticsException{

		// intrebariRepository.removeAllQuestions();
		
		if(intrebariRepository.getIntrebari().isEmpty())
			throw new NotAbleToCreateStatisticsException("Repository-ul nu contine nicio intrebare!");
		
		Statistica statistica = new Statistica();
		for(String domeniu : intrebariRepository.getDistinctDomains()){
			statistica.addStatistics(domeniu, intrebariRepository.getIntrebariByDomain(domeniu).size());
		}
		
		return statistica;
	}

}
