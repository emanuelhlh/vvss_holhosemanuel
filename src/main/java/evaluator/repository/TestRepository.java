package evaluator.repository;

import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.model.Test;
import evaluator.util.InputValidation;
import java.util.concurrent.ThreadLocalRandom;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class TestRepository {

    // private IntrebariRepository intrebariRepository;

    private List<Test> teste;

    public TestRepository(List<Test> teste, IntrebariRepository intrebariRepository)
    {
        this.teste = teste;
        // this.intrebariRepository = intrebariRepository;
    }

//    public void setIntrebariRepository(IntrebariRepository intrebariRepository) {
//        this.intrebariRepository = intrebariRepository;
//    }

    public TestRepository(){
        List<Test> testsToAdd = new LinkedList<>();
        this.setTeste(testsToAdd);
    }

    public void setTeste(List<Test> teste){
        this.teste = teste;
    }

    public List<Test> getTeste(){
        return this.teste;
    }

    public Test createTest(IntrebariRepository intrebariRepository) throws InputValidationFailedException {
        Test newTest = new Test();
        List<String> domeniiDiferite = new ArrayList<>();
        List<Intrebare> intrebari = new LinkedList<>();
        if((intrebariRepository.getIntrebari()==null)|| (intrebariRepository.getIntrebari().isEmpty())){
            throw new InputValidationFailedException("Nu exista intrebari adaugate!");
        } else if(intrebariRepository.getIntrebari().size()<5){
            throw new InputValidationFailedException("Trebuie sa existe cel putin 5 intrebari!");
        } else if(intrebariRepository.getNumberOfDistinctDomains()<5){
            throw new InputValidationFailedException("Trebuie sa existe intrebari din cel putin 5 domenii diferite");
        } else{
            domeniiDiferite.addAll(intrebariRepository.getDistinctDomains());
            for(int i = 0; i<5; i++){
                int randomNum = ThreadLocalRandom.current().nextInt(0, 4-i + 1);
                String domeniu = domeniiDiferite.get(randomNum);
                Intrebare intrebareToAdd = intrebariRepository.getIntrebariByDomain(domeniu).get(0);
                intrebari.add(intrebareToAdd);
                domeniiDiferite.remove(randomNum);
            }
            newTest.setIntrebari(intrebari);
            this.teste.add(newTest);
        }
        return newTest;
    }




}
