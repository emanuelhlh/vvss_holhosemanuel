package evaluator.repository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;


import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.util.InputValidation;

public class IntrebariRepository {

	private List<Intrebare> intrebari;
	
	public IntrebariRepository() {

        LinkedList<Intrebare> intrebari = new LinkedList<>();

        try {
            /**
             * Pre defined questions
             * Can be changed or deleted
             *
             * If we have some pre defined questions, a test can be created, using them, without having to add questions manually
             */
            Intrebare i1 = new Intrebare("Intrebare1?", "1)da", "2)nu", "3)nu stiu",1, "Geometrie");
            Intrebare i2 = new Intrebare("Intrebare2?", "1)da", "2)nu", "3)nu stiu",1, "Algebra");
            Intrebare i3 = new Intrebare("Intrebare3?", "1)da", "2)nu", "3)nu stiu",1, "Geometrie");
            Intrebare i4 = new Intrebare("Intrebare4?", "1)da", "2)nu", "3)nu stiu",1, "Algebra");

            intrebari.add(i1);
            intrebari.add(i2);
            intrebari.add(i3);
			intrebari.add(i4);

        } catch (InputValidationFailedException e) {
            e.printStackTrace();
        }

        setIntrebari(intrebari);
	}
	
//	public void addIntrebare(Intrebare i) throws DuplicateIntrebareException{
//		if(existsQuestion(i))
//			throw new DuplicateIntrebareException("Intrebarea deja exista!");
//		intrebari.add(i);
//	}

	public Intrebare addIntrebare(String enunt, String varianta1, String varianta2, String varianta3, Integer variantaCorecta, String domeniu) throws DuplicateIntrebareException, InputValidationFailedException {
		Intrebare intrebareDeAdaugat = new Intrebare();
		intrebareDeAdaugat.setVariantaCorecta(variantaCorecta);
		intrebareDeAdaugat.setVarianta3(varianta3);
		intrebareDeAdaugat.setVarianta2(varianta2);
		intrebareDeAdaugat.setVarianta1(varianta1);
		intrebareDeAdaugat.setEnunt(enunt);
		intrebareDeAdaugat.setDomeniu(domeniu);
		InputValidation.validateEnunt(enunt);
		InputValidation.validateVarianta1(varianta1);
		InputValidation.validateVarianta2(varianta2);
		InputValidation.validateVariantaCorecta(variantaCorecta);
		InputValidation.validateDomeniu(domeniu);
		if(existsQuestion(intrebareDeAdaugat)){
			throw new DuplicateIntrebareException("Intrebarea deja exista!");
		}
		intrebari.add(intrebareDeAdaugat);
		return intrebareDeAdaugat;
	}

	public void removeAllQuestions(){
		intrebari.clear();
	}
	
	public boolean existsQuestion(Intrebare i){
		for(Intrebare intrebare : intrebari)
			if(intrebare.equals(i))
				return true;
		return false;
	}
	
	public Intrebare pickRandomIntrebare(){
		Random random = new Random();
		return intrebari.get(random.nextInt(intrebari.size()));
	}
	
	public int getNumberOfDistinctDomains(){
		return getDistinctDomains().size();
		
	}
	
	public Set<String> getDistinctDomains(){
		Set<String> domains = new TreeSet<String>();
		for(Intrebare intrebre : intrebari)
			domains.add(intrebre.getDomeniu());
		return domains;
	}
	
	public List<Intrebare> getIntrebariByDomain(String domain){
		List<Intrebare> intrebariByDomain = new LinkedList<Intrebare>();
		for(Intrebare intrebare : intrebari){
			if(intrebare.getDomeniu().equals(domain)){
				intrebariByDomain.add(intrebare);
			}
		}
		
		return intrebariByDomain;
	}
	
	public int getNumberOfIntrebariByDomain(String domain){
		return getIntrebariByDomain(domain).size();
	}
	
	public List<Intrebare> loadIntrebariFromFile(String f){
		
		List<Intrebare> intrebari = new LinkedList<Intrebare>();
		BufferedReader br = null; 
		String line = null;
		List<String> intrebareAux;
		Intrebare intrebare;
		
		try{
			br = new BufferedReader(new FileReader(f));
			line = br.readLine();
			while(line != null){
				intrebareAux = new LinkedList<String>();
				while(!line.equals("##")){
					intrebareAux.add(line);
					line = br.readLine();
				}
				intrebare = new Intrebare();
				intrebare.setEnunt(intrebareAux.get(0));
				intrebare.setVarianta1(intrebareAux.get(1));
				intrebare.setVarianta2(intrebareAux.get(2));
				intrebare.setVariantaCorecta(Integer.valueOf(intrebareAux.get(4)));
				intrebare.setDomeniu(intrebareAux.get(5));
				intrebari.add(intrebare);
				line = br.readLine();
			}
		
		}
		catch (IOException e) {
			// TODO: handle exception
		}
		finally{
			try {
				if (br != null) {
					br.close();
				}
			} catch (IOException e) {
				// TODO: handle exception
			}
		}
		
		return intrebari;
	}
	
	public List<Intrebare> getIntrebari() {
		return intrebari;
	}

	public void setIntrebari(List<Intrebare> intrebari) {
		this.intrebari = intrebari;
	}
	
}
